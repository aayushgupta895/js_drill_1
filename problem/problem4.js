// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function problem4(inventory) {
  if (!Array.isArray(inventory)) {
    return `input is not an array`;
  }
  const inventoryLength = inventory.length;
  const carYearList = [];
  for (let index = 0; index < inventoryLength; index++) {
    carYearList[index] = inventory[index]["car_year"];
  }
  return carYearList.length == 0 ? `car's years not present` : carYearList;
}

module.exports = problem4;
