// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
"Car 33 is a *car year goes here* *car make goes here* *car model goes here*";

function problem1(carId, inventory) {
  if (!Array.isArray(inventory)) {
    return `input is not an array`;
  }

  const inventoryLength = inventory.length;

  for (let index = 0; index < inventoryLength; index++) {
    const carObj = inventory[index];
    if (typeof carObj == "object") {
      
      if (carObj["id"] == carId) {
        const carObjKeys = [];
        for (let key in carObj) {
          carObjKeys.push(key);
        }
        const ans =
          (carObjKeys.includes("car_make")
            ? ` car is a '${carObj["car_make"]}'`
            : ``) +
          (carObjKeys.includes("car_model")
            ? ` the  car model is '${carObj["car_model"]}'`
            : ``) +
          (carObjKeys.includes("car_year")
            ? ` car is made on the year '${carObj["car_year"]}'`
            : ``);
        return ans == `` ? `only car's id is present, no details found` : ans;
      }
    }
  }
  return `The car is not present`;
}

module.exports = problem1;
