// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function problem3(inventory) {
  if (!Array.isArray(inventory)) {
    return `input is not an array`;
  }
  const inventoryLength = inventory.length;
  const modelList = [];
  for (let index = 0; index < inventoryLength; index++) {
    modelList[index] = inventory[index]["car_model"];
  }
  sorting(modelList)
  return modelList;
}

function sorting(array) {
  const arrLen = array.length;
  let index, index2, temp;
  let swapped;
  for (index = 0; index <arrLen- 1; index++) {
    swapped = false;
    for (index2 = 0; index2 < arrLen - index - 1; index2++) {
      if (array[index2] > array[index2 + 1]) {
        temp = array[index2];
        array[index2] = array[index2 + 1];
        array[index2 + 1] = temp;
        swapped = true;
      }
    }
    if (swapped == false) break;
  }
}

module.exports = problem3;
