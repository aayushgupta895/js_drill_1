// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
"Last car is a *car make goes here* *car model goes here*";

function problem2(inventory) {
  if (!Array.isArray(inventory)) {
    return `input is not an array`;
  }
  const inventoryLength = inventory.length;
  const lastCar = inventory[inventoryLength - 1];
  if(typeof lastCar == 'object') {
    const carObjKeys = [];
    for( let key in lastCar){
      carObjKeys.push(key);
    }
    const ans = (carObjKeys.includes("car_make")
      ? `Last car is a '${lastCar["car_make"]}'`
      : `` ) + (carObjKeys.includes("car_model")
      ? ` the last car model is '${lastCar["car_model"]}'`
      : ``) + (carObjKeys.includes("car_year")
      ? `last car is made on the year '${lastCar["car_year"]}'`
      : ``);
    return ans == ``? `The car is not present` : ans;
  } else {
    return `The car is not present`;
  }

}

module.exports = problem2;
